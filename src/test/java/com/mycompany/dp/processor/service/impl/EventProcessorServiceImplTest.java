package com.mycompany.dp.processor.service.impl;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.MockitoAnnotations.initMocks;

import com.mycompany.dp.processor.jms.EventProcessorPojo;
import com.mycompany.dp.processor.model.ScreenData;
import com.mycompany.dp.processor.model.UserData;
import com.mycompany.dp.processor.repository.EventRepository;
import com.mycompany.dp.processor.repository.ScreenRepository;
import com.mycompany.dp.processor.repository.UserRepository;
import com.mycompany.dp.processor.service.internal.CacheService;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;


public class EventProcessorServiceImplTest {

  @Mock
  private EventRepository eventRepository;
  @Mock
  private UserRepository userRepository;
  @Mock
  private ScreenRepository screenRepository;

  private CacheService cacheService;

  private EventProcessorServiceImpl eventProcessorService;

  @Before
  public void init() {

    initMocks(this);

    cacheService = new CacheService(userRepository, screenRepository);
    eventProcessorService = spy(new EventProcessorServiceImpl(
        eventRepository, cacheService));
  }

  @Test
  public void should_processEvent() {
    final EventProcessorPojo event = getEventPojo();
    final UUID userId = UUID.randomUUID();
    final UUID screenId = UUID.randomUUID();

    given(userRepository.findByEmail(event.getEmail()))
        .willReturn(Optional.of(getUser(userId, event.getEmail())));
    given(screenRepository.findByName(event.getScreen()))
        .willReturn(Optional.of(getScreen(screenId, event.getScreen())));
    given(eventRepository.existsByRequestId(event.getRequestId()))
        .willReturn(false);
    given(eventRepository.existsByUserIdAndClickTime(userId, event.getClickTime()))
        .willReturn(false);

    assertThat(eventProcessorService.processEvent(event)).isNotNull();

    then(eventRepository).should().existsByRequestId(event.getRequestId());
    then(eventRepository).should()
        .existsByUserIdAndClickTime(userId, event.getClickTime());
    then(userRepository).should().findByEmail(event.getEmail());
    then(screenRepository).should().findByName(event.getScreen());

    then(eventRepository).should().save(any());
  }

  @Test
  public void should_Fail_processEvent_ExistByRequestId() {
    final EventProcessorPojo event = getEventPojo();
    final UUID userId = UUID.randomUUID();
    final UUID screenId = UUID.randomUUID();

    given(userRepository.findByEmail(event.getEmail()))
        .willReturn(Optional.of(getUser(userId, event.getEmail())));
    given(screenRepository.findByName(event.getScreen()))
        .willReturn(Optional.of(getScreen(screenId, event.getScreen())));
    given(eventRepository.existsByRequestId(event.getRequestId()))
        .willReturn(true);
    given(eventRepository.existsByUserIdAndClickTime(userId, event.getClickTime()))
        .willReturn(false);

    assertThat(eventProcessorService.processEvent(event)).isNull();

    then(eventRepository).should().existsByRequestId(event.getRequestId());
    then(eventRepository).should(never())
        .existsByUserIdAndClickTime(userId, event.getClickTime());
    then(userRepository).should(never()).findByEmail(event.getEmail());
    then(screenRepository).should(never()).findByName(event.getScreen());
  }

  @Test
  public void should_Fail_processEvent_ExistByEmailTime() {
    final EventProcessorPojo event = getEventPojo();
    final UUID userId = UUID.randomUUID();
    final UUID screenId = UUID.randomUUID();

    given(userRepository.findByEmail(event.getEmail()))
        .willReturn(Optional.of(getUser(userId, event.getEmail())));
    given(screenRepository.findByName(event.getScreen()))
        .willReturn(Optional.of(getScreen(screenId, event.getScreen())));

    given(eventRepository.existsByRequestId(event.getRequestId()))
        .willReturn(false);
    given(eventRepository.existsByUserIdAndClickTime(userId, event.getClickTime()))
        .willReturn(true);

    assertThat(eventProcessorService.processEvent(event)).isNull();

    then(eventRepository).should().existsByRequestId(event.getRequestId());
    then(eventRepository).should()
        .existsByUserIdAndClickTime(userId, event.getClickTime());
    then(userRepository).should().findByEmail(event.getEmail());
    then(screenRepository).should(never()).findByName(event.getScreen());
  }

  private EventProcessorPojo getEventPojo() {
    return EventProcessorPojo.builder()
        .email("a@b.c")
        .clickTime(LocalDateTime.now())
        .requestId("id")
        .build();
  }

  private UserData getUser(UUID userId, String email) {
    final UserData userData = UserData.builder()
        .email(email)
        .build();

    userData.setId(userId);

    return userData;
  }

  private ScreenData getScreen(UUID screenId, String name) {
    final ScreenData screenData = ScreenData.builder()
        .name(name)
        .build();

    screenData.setId(screenId);

    return screenData;
  }
}
