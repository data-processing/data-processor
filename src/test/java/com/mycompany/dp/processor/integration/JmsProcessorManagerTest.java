package com.mycompany.dp.processor.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.dp.processor.ProcessorApplication;
import com.mycompany.dp.processor.jms.EventAnalyzerPojo;
import com.mycompany.dp.processor.jms.EventProcessorPojo;
import com.mycompany.dp.processor.jms.JmsProcessorManager;
import com.mycompany.dp.processor.model.EventData;
import com.mycompany.dp.processor.model.ScreenData;
import com.mycompany.dp.processor.model.UserData;
import com.mycompany.dp.processor.repository.EventRepository;
import com.mycompany.dp.processor.repository.ScreenRepository;
import com.mycompany.dp.processor.repository.UserRepository;
import com.mycompany.dp.processor.service.EventProcessorService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
    ProcessorApplication.class}, properties = "spring.profiles.active:test")
public class JmsProcessorManagerTest {

  @Autowired
  private EventRepository eventRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ScreenRepository screenRepository;

  @Autowired
  private JmsProcessorManager jmsProcessorManager;

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private JmsTemplate jmsTemplate;

  @SpyBean
  private EventProcessorService eventProcessorService;

  @Test
  public void should_processEvent() throws JsonProcessingException {
    final EventProcessorPojo event1 = getEventPojo("a@b.c", UUID.randomUUID().toString());
    final EventProcessorPojo event2 = getEventPojo("d@e.f", UUID.randomUUID().toString());

    doNothing().when(jmsTemplate).convertAndSend(isA(String.class), isA(EventAnalyzerPojo.class));

    jmsProcessorManager.processEvent(objectMapper.writeValueAsString(event1));
    then(eventProcessorService).should().processEvent(event1);

    jmsProcessorManager.processEvent(objectMapper.writeValueAsString(event2));
    then(eventProcessorService).should(times(2)).processEvent(any());

    final List<EventData> events = new ArrayList<>();
    eventRepository.findAll().forEach(e -> events.add(e));

    assertThat(events).hasSize(2);
    assertThat(events.stream().map(e -> e.getRequestId()).collect(Collectors.toList()))
        .containsExactlyInAnyOrder(event1.getRequestId(), event2.getRequestId());

    final List<UserData> users = new ArrayList<>();
    userRepository.findAll().forEach(e -> users.add(e));

    assertThat(users).hasSize(2);
    assertThat(users.stream().map(e -> e.getEmail()).collect(Collectors.toList()))
        .containsExactlyInAnyOrder(event1.getEmail(), event2.getEmail());

    final List<ScreenData> screens = new ArrayList<>();
    screenRepository.findAll().forEach(e -> screens.add(e));

    assertThat(screens).hasSize(1);
    assertThat(screens.stream().map(e -> e.getName()).collect(Collectors.toList()))
        .containsExactlyInAnyOrder(event1.getScreen());
  }


  private EventProcessorPojo getEventPojo(String email, String requestId) {
    return EventProcessorPojo.builder()
        .email(email)
        .name(UUID.randomUUID().toString())
        .screen("Options")
        .clickTime(LocalDateTime.now())
        .requestId(requestId)
        .build();
  }

}
