package com.mycompany.dp.processor.repository;

import com.mycompany.dp.processor.model.UserData;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserData, String> {

  Optional<UserData> findByEmail(String email);

}
