package com.mycompany.dp.processor.repository;

import com.mycompany.dp.processor.model.ScreenData;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface ScreenRepository extends CrudRepository<ScreenData, String> {

  Optional<ScreenData> findByName(String name);

}
