package com.mycompany.dp.processor.repository;

import com.mycompany.dp.processor.model.EventData;
import java.time.LocalDateTime;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository to handle CRUD operations for event_data table.
 */
public interface EventRepository extends CrudRepository<EventData, String> {

  boolean existsByRequestId(String requestId);

  boolean existsByUserIdAndClickTime(UUID userId, LocalDateTime clickTime);

}
