package com.mycompany.dp.processor.jms;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * POJO to send data by JMS to Data Analyzer.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EventAnalyzerPojo implements Serializable {

  public static final long serialVersionUID = 1L;

  private String requestId;

  private UUID userId;
  private String email;
  private String name;

  private UUID screenId;
  private String screen;

  private LocalDateTime clickTime;

}
