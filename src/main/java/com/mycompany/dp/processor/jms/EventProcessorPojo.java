package com.mycompany.dp.processor.jms;

import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * POJO to get data by JMS from Data Processor.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EventProcessorPojo implements Serializable {

  public static final long serialVersionUID = 1L;

  private String requestId;

  private String email;

  private String name;

  private String screen;

  private LocalDateTime clickTime;

}
