package com.mycompany.dp.processor.jms;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.dp.processor.service.EventProcessorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Slf4j
@Component
public class JmsProcessorManager {

  private final EventProcessorService eventProcessorService;
  private final JmsTemplate jmsTemplate;
  private final ObjectMapper objectMapper;
  @Value("${processor.analyzer.queueName:analyzer}")
  private String queueName;

  @JmsListener(destination = "${processor.queueName:processor}",
      containerFactory = "jmsListenerContainerFactory")
  public void processEvent(String eventData) throws JsonProcessingException {
    final EventProcessorPojo event = objectMapper.readValue(eventData, EventProcessorPojo.class);
    MDC.put("requestId", event.getRequestId());
    final EventAnalyzerPojo eventAnalyzerPojo = eventProcessorService.processEvent(event);
    if (eventAnalyzerPojo != null) {
      sendEventToAnalyzer(eventAnalyzerPojo);
    }
  }


  public void sendEventToAnalyzer(EventAnalyzerPojo event) {
    try {
      log.info("Sending an event to processor: {}", event);

      jmsTemplate.convertAndSend(queueName, objectMapper.writeValueAsString(event));

      log.info("Successfully sent an event to processor: {}", event);
    } catch (Exception ex) {
      log.error("Exception occurred while sending event to analyzer. Message: {}", ex.getMessage());
    }
  }

  @Bean
  public AmazonSQS amazonSQSClient(AWSCredentialsProvider credentialsProvider,
      @Value("${cloud.aws.region.static}") String region) {
    return AmazonSQSClientBuilder.standard()
        .withCredentials(credentialsProvider)
        .withRegion(region)
        .build();
  }

}
