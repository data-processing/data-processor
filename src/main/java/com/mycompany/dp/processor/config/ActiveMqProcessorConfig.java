package com.mycompany.dp.processor.config;

import javax.jms.ConnectionFactory;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;

/**
 * JMS configuration for ActiveMQ.
 */
@Configuration
@EnableJms
@Profile("dev")
public class ActiveMqProcessorConfig {

  /**
   * JMS Factory bean.
   *
   * @param connectionFactory {@link ConnectionFactory}
   * @param configurer {@link DefaultJmsListenerContainerFactoryConfigurer}
   * @return {@link DefaultJmsListenerContainerFactory}
   */
  @Bean
  public DefaultJmsListenerContainerFactory jmsListenerContainerFactory(
      ConnectionFactory connectionFactory,
      DefaultJmsListenerContainerFactoryConfigurer configurer) {
    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
    // This provides all boot's default to this factory, including the message converter
    configurer.configure(factory, connectionFactory);
    // You could still override some of Boot's default if necessary.
    return factory;
  }
}
