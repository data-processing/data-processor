package com.mycompany.dp.processor.service.internal;

import com.mycompany.dp.processor.jms.EventProcessorPojo;
import com.mycompany.dp.processor.model.ScreenData;
import com.mycompany.dp.processor.model.UserData;
import com.mycompany.dp.processor.repository.ScreenRepository;
import com.mycompany.dp.processor.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CacheService {

  private final UserRepository userRepository;
  private final ScreenRepository screenRepository;

  @Cacheable(value = "userCache", key = "#event.email")
  public UserData getUserOfEvent(EventProcessorPojo event) {
    return userRepository.findByEmail(event.getEmail())
        .orElseGet(() -> {
          try {
            return userRepository.save(UserData.from(event));
          } catch (Exception ex) {
            // if the concurrent process already saved return again
            return userRepository.findByEmail(event.getEmail())
                .orElseThrow(() -> new RuntimeException("Cannot connect to storage"));
          }
        });
  }


  @Cacheable(value = "screenCache", key = "#event.screen")
  public ScreenData getScreenOfEvent(EventProcessorPojo event) {
    return screenRepository.findByName(event.getScreen())
        .orElseGet(() -> {
          try {
            return screenRepository.save(ScreenData.from(event));
          } catch (Exception ex) {
            // if the concurrent process already saved return again
            return screenRepository.findByName(event.getScreen())
                .orElseThrow(() -> new RuntimeException("Cannot connect to storage"));

          }
        });
  }

}
