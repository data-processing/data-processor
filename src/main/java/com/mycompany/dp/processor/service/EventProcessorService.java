package com.mycompany.dp.processor.service;

import com.mycompany.dp.processor.jms.EventAnalyzerPojo;
import com.mycompany.dp.processor.jms.EventProcessorPojo;

/**
 * Defines service interface to process event.
 */
public interface EventProcessorService {

  EventAnalyzerPojo processEvent(EventProcessorPojo event);
}
