package com.mycompany.dp.processor.service.impl;

import com.mycompany.dp.processor.jms.EventAnalyzerPojo;
import com.mycompany.dp.processor.jms.EventProcessorPojo;
import com.mycompany.dp.processor.model.EventData;
import com.mycompany.dp.processor.model.ScreenData;
import com.mycompany.dp.processor.model.UserData;
import com.mycompany.dp.processor.repository.EventRepository;
import com.mycompany.dp.processor.service.EventProcessorService;
import com.mycompany.dp.processor.service.internal.CacheService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service to filter nad normalize event data.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class EventProcessorServiceImpl implements EventProcessorService {

  private final EventRepository eventRepository;
  private final CacheService cacheService;

  @Override
  @Transactional
  public EventAnalyzerPojo processEvent(EventProcessorPojo event) {
    if (eventRepository.existsByRequestId(event.getRequestId())) {
      log.error("Found duplicate request");

      return null;
    }

    final UserData userData = cacheService.getUserOfEvent(event);
    if (eventRepository.existsByUserIdAndClickTime(userData.getId(), event.getClickTime())) {
      log.error("User with email {} already clicked at time {}",
          event.getEmail(), event.getClickTime());

      return null;
    }

    final ScreenData screenData = cacheService.getScreenOfEvent(event);
    saveEvent(event, userData, screenData);

    return EventAnalyzerPojo.builder()
        .email(event.getEmail())
        .name(event.getName())
        .screen(event.getScreen())
        .clickTime(event.getClickTime())
        .requestId(event.getRequestId())
        .screenId(screenData.getId())
        .userId(userData.getId())
        .build();
  }

  private EventData saveEvent(EventProcessorPojo event, UserData userData, ScreenData screenData) {
    final EventData eventData = EventData.builder()
        .requestId(event.getRequestId())
        .clickTime(event.getClickTime())
        .screenId(screenData.getId())
        .userId(userData.getId())
        .build();

    return eventRepository.save(eventData);
  }
}
