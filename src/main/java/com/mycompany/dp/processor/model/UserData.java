package com.mycompany.dp.processor.model;

import com.mycompany.dp.processor.jms.EventProcessorPojo;
import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserData extends AbstractEntity {

  @Column(nullable = false)
  private String email;

  @Column
  private String name;

  public static UserData from(EventProcessorPojo event) {
    return UserData.builder()
        .email(event.getEmail())
        .name(event.getName())
        .build();
  }
}
