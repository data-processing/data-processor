package com.mycompany.dp.processor.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import lombok.Data;

/**
 * Defines base class for entity.
 */
@Data
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(nullable = false, updatable = false)
  private UUID id;

  @Column(name = "created_time", nullable = false, updatable = false)
  private LocalDateTime createdTime;

  /**
   * Set PK and created time before save.
   */
  @PrePersist
  public void prePersist() {
    if (id == null) {
      this.id = UUID.randomUUID();
    }
    if (createdTime == null) {
      createdTime = LocalDateTime.now();
    }
  }

}
