package com.mycompany.dp.processor.model;

import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Entity to keep record about user click.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class EventData extends AbstractEntity {

  @Column(name = "request_id", nullable = false, updatable = false)
  private String requestId;

  @Column(name = "user_id", nullable = false, updatable = false)
  private UUID userId;

  @Column(name = "screen_id", nullable = false, updatable = false)
  private UUID screenId;

  @Column(name = "click_time", nullable = false, updatable = false)
  private LocalDateTime clickTime;
}
