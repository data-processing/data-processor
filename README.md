# Data Processor

Data Processor: reads raw data from data lake, aggregate, normalize and store in the persistent database.

For the further information please refer to the platform [official documentation](https://gitlab.com/data-processing/data-processing-meta/-/blob/master/README.md)  
 
