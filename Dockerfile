FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn clean package -Dmaven.test.skip=true

ENV SERVER_PORT 8012

EXPOSE $SERVER_PORT

CMD [ "sh", "-c", "mvn -Dserver.port=${SERVER_PORT} spring-boot:run" ]